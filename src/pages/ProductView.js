import { Row, Col, Container, Button, Card, Form, InputGroup} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link }  from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView(){

    const navigate = useNavigate ();
    const {productId} = useParams();
    const { user } = useContext(UserContext);

   

    const [userId, setUserId] = useState(localStorage.getItem("userId"));

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [totalAmount, setTotalAmount] = useState(0);
    const [newStock, setNewStock] = useState(0);
    
    const [orderQuantity, setOrderQuantity] = useState(1);

    useEffect(() => {
        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProductName(data.productName);
            setDescription(data.description);
            setPrice(data.price);
            
        })
    }, [productId])

    const addtocart = (productId) => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/addtocart`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId : productId,
                quantity:orderQuantity,
                totalAmount: totalAmount
            })

        })
        .then(res => res.json())
        .then(data => {

        console.log("add to cart data");
        console.log(data);
            if(data){
                    Swal.fire({
                        title: "Successfully Purchase",
                        icon: "success",
                        text: "Thank you for purchasing at our Store!"
                    })
                    navigate("/Products")        
             }

             else{
                Swal.fire({
                    title: "Error Failed",
                    icon: "error",
                    text: "Sorry! No stocks available at the moment"
                })
            }
         } )  
        }
         
    return(
        <Container className="mt-5">
           <Row>
            <Col lg={{span:6, offset: 3}}>
                <card>
                <Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							{/* <Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>

                            <Card.Subtitle>quantity:</Card.Subtitle>
							<Card.Text>{orderQuantity}</Card.Text> */}
							
							{
								(user.id !== null || userId !== null)
								?
                                <>
                                <InputGroup onSubmit={(e => addtocart(productId))}>
                                    <Form.Control 
                                    aria-label="Dollar amount (with dot and two decimal places)"
                                    placeholder='Place the Quantity'
                                    value={orderQuantity}
                                    onChange= {e => setOrderQuantity(e.target.value)}
                                    required
                                     />
                                    <InputGroup.Text>₱</InputGroup.Text>
                                    <InputGroup.Text>{totalAmount}</InputGroup.Text>
                                </InputGroup>
                                <Button className='mt-3' variant="primary"  size="lg" onClick={() => addtocart(productId)}>Purchase</Button>
                             </>
								:
									<Button as={Link} to="/Login" variant="success"  size="lg">Login to Purchase</Button>
							}
						</Card.Body>		
                </card>

            </Col>
           </Row> 
        </Container>
    )
}

